package com.carrefour.delivery;

public enum DeliveryMethod {
	// Enumération des méthodes de livraison disponibles
	    DRIVE,
	    DELIVERY,
	    DELIVERY_TODAY,
	    DELIVERY_ASAP
	
}
